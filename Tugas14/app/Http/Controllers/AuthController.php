<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function signup()
    {
        return view('register');
    }

    public function kirim(Request $request)
    {
        $firstname = $request['firstname'];
        $lastname = $request['lastname'];

        return view('welcome', ['firstname' => $firstname, 'lastname' => $lastname]);
    }
}
