@extends('layout.master')

@section('judul')
    Register
@endsection

@section('content')
<h1>Buat Account Baru!</h1>
<h3>Sign Up Form</h3>

<form action="/kirim" method="post">
    @csrf
    <label>First Name:</label> <br> <br>
    <input type="text" name="firstname"> <br> <br>

    <label>Last Name:</label> <br> <br>
    <input type="text" name="lastname"> <br> <br>

    <label>Gender</label> <br> <br>
    <input type="radio" name="gender">Male <br>
    <input type="radio" name="gender">Female <br>
    <input type="radio" name="gender">Other <br> <br>

    <label>Nationality</label> <br> <br>
    <select name="Nationality">
        <option value="">Indonesian</option>
        <option value="">Singaporean</option>
        <option value="">Malaysian</option>
        <option value="">Australian</option>
    </select> <br> <br>

    <label>Language Spoken:</label> <br> <br>
    <input type="checkbox" name="language">Bahasa Indonesia <br>
    <input type="checkbox" name="language">English <br>
    <input type="checkbox" name="language">Other <br> <br>

    <label>Bio:</label> <br> <br>
    <textarea cols="30" rows="10"></textarea> <br> <br>

    <input type="submit" value="Sign Up"></input>
</form>
@endsection
    
